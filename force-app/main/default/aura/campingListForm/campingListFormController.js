({
	clickCreateItem: function(component, event, helper) {
        var validExpense = component.find('campform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        // If we pass error checking, do some real work
        if(validExpense){
            // Create the new expense
            var newItem = component.get("v.newItem");
            console.log("Create expense: " + JSON.stringify(newItem));
            //helper.createExpense(component, newItem);
            helper.createItem(component, newItem);
            //var theCamps = component.get("v.items");
            //var newCamp = JSON.parse(JSON.stringify(newItem));
 
            //theCamps.push(newCamp);
            //component.set("v.items", theCamps);
            //newItem.push(new Camping_Item__c);
            //component.set("v.newItem", new Camping_Item__c );
            /* component.set("v.newItem",{'sobjectType':'Camping_Item__c',
                'Name': '',
                'Quantity__c': 0,
                'Price__c': 0,
                'Packed__c': false}); */
            
        }
    },
    
    addItem: function(component, newItem) {
    helper.createItem(component, newItem);    
    //var createEvent = component.getEvent("addItem");
    //createEvent.setParams({ "item": newItem });
    //createEvent.fire();
},
})