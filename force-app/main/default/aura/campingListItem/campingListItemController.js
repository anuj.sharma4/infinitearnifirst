({
	packItem : function(component, event, helper) {
    
    var campingListItem = component.get("v.item");
    campingListItem.packed__c = true;
    component.set("v.item", campingListItem); 
    event.getSource().set("v.disabled", true);      
    
}
})