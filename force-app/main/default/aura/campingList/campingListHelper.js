({
    createItem: function(component, Item) {
    var action = component.get("c.saveItem");
    action.setParams({
        "campItem": Item
    });
    action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            var Items = component.get("v.items");
            Items.push(response.getReturnValue());
            component.set("v.items", Items);
        }
    });
    $A.enqueueAction(action);
},
})