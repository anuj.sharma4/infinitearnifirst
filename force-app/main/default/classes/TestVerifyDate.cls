@isTest
private class TestVerifyDate {
    @isTest static void test1() {
    
        Date myDate1 = Date.newInstance(2018, 3, 15);
        Date myDate2 = mydate1.addDays(15);
       
        Date retDate = VerifyDate.CheckDates(myDate1, myDate2);
        System.assertEquals(retDate,myDate2);
    }
    
    
    @isTest static void test2() {
    
        Date myDate1 = Date.newInstance(2018, 3, 15);
        Date myDate2 = mydate1.addDays(35);
        
        Date lastDay = Date.newInstance(2018, 3, 31);
        
        Date retDate = VerifyDate.CheckDates(myDate1, myDate2);
        System.assertEquals(retDate,lastDay);
        
    }
    
      
}