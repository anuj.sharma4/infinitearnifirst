global class DailyLeadProcessor implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Lead> Leads = [SELECT Id, Name, LeadSource From Lead Where LeadSource = '' OR LeadSource = NULL LIMIT 200];
         for (Lead leadObj : Leads) {
              leadObj.LeadSource = 'Dreamforce';
         
         }
         
         Update Leads;
    }
    
}