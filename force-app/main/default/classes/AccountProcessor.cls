public class AccountProcessor {

    @future
    public static void countContacts(list<Id> AccIdList) {

         Integer contCount;
         Map<Id, Integer> AccCountMap = new Map<Id, Integer>();
         List<Account> UpdateAccList = new List<Account>();
         
         List<Account> AccList = [SELECT a.Id, a.Number_of_Contacts__c,(SELECT Id from a.Contacts) FROM Account a WHERE Id IN :AccIdList];         
         
         for(Account accObj : AccList) {            
             contCount = 0;
             
             for(Contact contObj : accObj.Contacts) {
                 contCount++;
             
             }
             
             //AccCountMap.put(accObj.Id, contCount); 
             accObj.Number_of_Contacts__c = contCount;
             UpdateAccList.add(accObj);
          } 
          
          if(UpdateAccList.size() > 0)
             update UpdateAccList;

    }   
}