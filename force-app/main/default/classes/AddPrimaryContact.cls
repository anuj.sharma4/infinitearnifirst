public class AddPrimaryContact implements Queueable { 
    
    private Contact ContactObj;
    private String State;
    
    public AddPrimaryContact(Contact contObj, String StateStr) {
        this.ContactObj = contObj;
        this.State = StateStr;
    }
    
    public void execute(QueueableContext context) {
        
       List<Contact> contacts = new List<Contact>();
        List<account> accounts = [select id, BillingState from account where billingstate = : State LIMIT 200];        
        
       //Integer i=1;
        for (Account account : accounts) {
        
           Contact clonedCon = ContactObj.clone(false, false, false, false);
           clonedCon.accountId =account.id;

           //contacts.add(new Contact(firstname='first '+i, lastname='last '+i, accountId=account.id));
           contacts.add(clonedCon);
           //i++;
        }
        Insert contacts;
    }
    
}