global with sharing class CaseService {

      global static void closeCases(Set<Id> CaseIdSet, String closeReason) {
          
          // Query Opportunities and Lines (SOQL inlined for this example, see Selector pattern in later module)
          List<Case> Cases =
            [select Id, Status, Reason  from Case where Id in :CaseIdSet];
        
          List<Case> casesToUpdate = new List<Case>();
          
          for(Case caseObj : Cases) {
              caseObj.status = 'Closed';
              caseObj.reason = closeReason;
              
              casesToUpdate.add(caseObj);
              
          }
             
          // Update the database
        SavePoint sp = Database.setSavePoint();
        try {
            update casesToUpdate;
        } catch (Exception e) {
            // Rollback
            Database.rollback(sp);
            // Throw exception on to caller
            throw e;
        }       
                   
    }
    
}