@isTest
public class UnitOfWorkTest {
    
    static testmethod void challengeComplete()
    {
        
        // Create a Unit Of Work
		fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
    									new Schema.SObjectType[] {
        								Account.SObjectType,
                                        Contact.SObjectType,
                                        Note.SObjectType
                                    });

        
        for(Integer o=0; o<100; o++) 
        {
            Account accObj = new Account();
            accObj.Name = 'UoW Test Name ' + o;
            uow.registerNew(accObj);
            
        }
            
         for(Integer o=0; o<500; o++) 
         {
            Contact cObj = new Contact();
            cObj.LastName  = 'UoW Test Contact Name ' + o;
            uow.registerNew(cObj);
            //for(Integer i=0; i<o+1; i++) 
              //{
                Note  nObj = new Note();
                nObj.Title   = 'UoW Test Note Title ' + o;
                uow.registerNew(nObj, Note.ParentId , cObj);
              // }
        
          }
        
          // Commit the work to the database!
		  uow.commitWork();
        
          System.assertEquals(100, [Select Id from Account].size());
		  System.assertEquals(500, [Select Id from Contact].size());
          System.assertEquals(500, [Select Id from Note].size());
        
        
        
    }

}