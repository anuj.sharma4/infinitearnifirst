@isTest
public class AddPrimaryContactTest {
    @testSetup 
    static void setup() {
    
       List<Account> accounts = new List<Account>();
       
        // insert 50 accounts with NY
        for (Integer i=0;i<50;i++) {
            accounts.add(new Account(name='Account '+i, billingstate = 'NY', 
                billingcity='New York', billingcountry='USA'));
        }
        
        // insert 50 accounts with CA
        for (Integer i=0;i<50;i++) {
            accounts.add(new Account(name='Account '+i, billingstate = 'CA', 
                billingcity='California', billingcountry='USA'));
        }
        
        
        insert accounts;
    }
    
    static testmethod void testQueueable() {
        Contact conObj = new Contact(firstname='first', lastname='last');
        // query for test data to pass to queueable class
        //id parentId = [select id from account where name = 'Parent'][0].Id;
        //List<Account> accounts = [select id, name from account where name like 'Test Account%'];
        // Create our Queueable instance
        AddPrimaryContact updater = new AddPrimaryContact(conObj, 'CA');
        // startTest/stopTest block to force async processes to run
        Test.startTest();        
        System.enqueueJob(updater);
        Test.stopTest();        
        // Validate the job ran. Check if record have correct parentId now
        System.assertEquals(50, [select count() from Contact where Account.billingstate = 'CA']);
    }
    
}