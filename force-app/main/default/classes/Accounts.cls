public class Accounts extends fflib_SObjectDomain {
    public Accounts(List<Account> sObjectList) {
        super(sObjectList);
    }
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Accounts(sObjectList);
        }
    }
    
   public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
       
       String DomStr = 'Domain classes rock!';
        
        for(Account accOBJ : (List<Account>) Records) 
        {
        	//Account accUPD = (Account)existingRecords.get(accOBJ.Id);          
            if(accOBJ != null && accOBJ.Description != null ){
            	String s = accOBJ.Description;
            	Integer i = DomStr.getLevenshteinDistance(s);
                
                accOBJ.AnnualRevenue = i;               
            }

        }   
               
   }
 
    
    
    public override void onApplyDefaults() 
    {
        // Apply defaults to Opportunities
        for(Account accOBJ : (List<Account>) Records) {
               accOBJ.Description = 'Domain classes rock!';           
        }
     }

    
}