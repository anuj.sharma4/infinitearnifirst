@isTest
private class AccountProcessorTest {
    @isTest static void test1() {
        
        List<Account> accts = new List<Account>();
        List<Id> accIDs = new List<Id>();
        List<Contact> contacts = new List<Contact>();
        
        for(integer i=0 ; i<= 3; i++){
           String TestName = 'Test Account '+i+'';
           Account a = new Account(Name= TestName );
           accts.add(a);
        }
                
        Insert accts;

        for( Account AccObj : accts){
        
           String TestName1 = 'Test Contact';
           Contact c = new Contact(LastName = TestName1, AccountId = AccObj.id );
           contacts.add(c);
           
           accIDs.add(AccObj.Id);
        }
        
        Insert contacts;
        
        Test.startTest();        
        AccountProcessor.countContacts(accIDs);
        Test.stopTest();
           
    }
}