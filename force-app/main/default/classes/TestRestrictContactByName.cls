@isTest
private class TestRestrictContactByName {
    @isTest static void Test1() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        //Account acct = new Account(Name='Test Account');
        //insert acct;        
        
        Contact cont = new Contact(LastName='INVALIDNAME'); 
        string errmsg = 'The Last Name "'+cont.LastName+'" is not allowed for DML'; 
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(cont, false);
        Test.stopTest();
        // Verify 
        // In this case the Insert should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals(errmsg, result.getErrors()[0].getMessage());
    }
    
    @isTest static void Test2() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        //Account acct = new Account(Name='Test Account');
        //insert acct;
        
        Contact cont = new Contact(LastName='Valid Name');
        
        insert cont;   
        // Perform test
        Test.startTest();
        cont.LastName = 'INVALIDNAME';
        string errmsg = 'The Last Name "'+cont.LastName+'" is not allowed for DML'; 
        Database.SaveResult result = Database.update(cont, false);
        Test.stopTest();
        // Verify 
        // In this case the Insert should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals(errmsg, result.getErrors()[0].getMessage());                             
    }
    
}