@isTest
private class LeadProcessorTest {
    @testSetup 
    static void setup() {
        List<Lead> leadList = new List<Lead>();
        // insert 10 Leads
        for (Integer i=0;i<10;i++) {
            leadList.add(new Lead(firstname='first ' +i, lastname='Last '+i, 
                company='New York', Status ='Open - Not Contacted'));
        }
        insert leadList;
        
    }
    static testmethod void test() {        
        Test.startTest();
        LeadProcessor lpObj = new LeadProcessor();
        Id batchId = Database.executeBatch(lpObj);
        Test.stopTest();
        // after the testing stops, assert records were updated properly
        System.assertEquals(10, [select count() from Lead where LeadSource = 'Dreamforce']);
    }
    
}