trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {
    
    List<BatchLeadConvertErrors__c> ObjInsertList = new List<BatchLeadConvertErrors__c>();
    for(BatchApexErrorEvent evt: Trigger.new){
        BatchLeadConvertErrors__c obj = new BatchLeadConvertErrors__c();
        obj.AsyncApexJobId__c = evt.AsyncApexJobId;
        obj.Records__c = evt.JobScope ;
        obj.StackTrace__c = evt.StackTrace;
        
        ObjInsertList.add(obj);
    }
    
    insert ObjInsertList;    
  
}