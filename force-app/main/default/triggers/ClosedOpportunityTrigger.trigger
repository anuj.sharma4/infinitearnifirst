trigger ClosedOpportunityTrigger on Opportunity (before insert, before update) {
    
    List<task> tlist = new List<task>();
    
    for(Opportunity o : Trigger.New) 
    { 
        //if(Trigger.oldMap.get(a.Id))
        if(Trigger.isInsert ||(Trigger.isUpdate && Trigger.oldMap.get(o.Id).StageName != o.StageName ))
        {
             if(o.StageName  == 'Closed Won')
             {
                Task t = new task();
                t.WhatId  = o.id;
                t.Subject  = 'Follow Up Test Task';
                
                tlist.add(t);
       
            }             
        }          
    }
    
    if (tlist.size() > 0) 
    	insert tlist;
    
    


}